module.exports = function(grunt) {

  grunt.initConfig({
    project: {
      name: "initcomponents"
    },
    uglify: {
      def : {
        files:
          {
            '<%= project.name %>.min.js' : 'src/<%= project.name %>.js'
          }
        }
    },
    watch: {
      def: {
        files: ['src/*.js']
      , tasks: ['uglify']
      , options : {
          spawn : false,
          livereload: {
            port: 9000
          }
        }
      }
    }
  });

  grunt.loadNpmTasks('grunt-contrib-uglify');
  // grunt.loadNpmTasks('grunt-contrib-concat');
  //grunt.loadNpmTasks('grunt-contrib-cssmin');
  //grunt.loadNpmTasks('grunt-contrib-watch');
  //grunt.loadNpmTasks('grunt-contrib-stylus');
  //grunt.loadNpmTasks('grunt-newer');

  grunt.registerTask('default', ['watch:def']);
  grunt.registerTask('js-task', ['uglify']);
  //grunt.registerTask('css-task', ['stylus','cssmin']);
  // grunt.registerTask('stylus-task', ['stylus']);
};